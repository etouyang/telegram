
const TelegramBot = require('node-telegram-bot-api');
const token = '628987846:AAF79q7qlO7eysH9LzoyrSijjmgT0fWURgc';

const bot = new TelegramBot(token, {polling: true})
const groupId = '-1001228652453';

const me = '574201003';
const wg1 = '-1001346842188' // DC Lab
const wg2 = '-1001213269564' // DRDC Stage
const wg3 = '-1001291931092' // DECENT Stage
const wg4 = '-1001079860252' // DECENT Official
const wg5 = '-1001143521801' // DECENT China
const wg6 = '-1001250495481' // ALAX.io



const redis = require('redis');
client = redis.createClient();
client.on("error", function (err) {
    console.log("Error " + err);
});


bot.onText(/\/groupid/, (msg, match) => {
    let chatId = msg.chat.id; //group的ID

    console.log('invited check');
    client.hgetall('test01', (err, obj)=> {
        console.log(obj)
    })
});






/**目录**/
bot.onText(/^\/info/, (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id; //group的ID
    let info = '试试这些命令吧：\n/wallet\n/how\n/decent\n/media\n/price\n/help';
    if (msg.chat.id == wg4 || msg.chat.id == groupId) {
        info = 'Please Try：\n/wallet\n/decent\n/media\n/price\n/help\n/exchanges'
    }
    bot.sendMessage(chatId, info, {disable_web_page_preview:true});
});

/**钱包**/
bot.onText(/^\/wallet/, (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let info = 'Web wallet：https://wallet.decent.ch/\n电脑客户端：https://decent.ch/dcore/';
    if (msg.chat.id == wg4 || msg.chat.id == groupId) {
        info = 'Web wallet：https://wallet.decent.ch/\nMobile & web-based clients：https://decent.ch/dcore/';
    }
    bot.sendMessage(chatId, info, {disable_web_page_preview:true})
});

/**钱包的使用方式**/
bot.onText(/^\/how/, (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let info = 'Mac OS版客户端下载&使用指南：https://goo.gl/jYagMv';
    if (msg.chat.id == wg4 || msg.chat.id == groupId) {
        info = 'coming soon';
    }
    bot.sendMessage(chatId, info, {disable_web_page_preview:true});
});

/**项目简介**/
bot.onText(/^\/decent/, (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let info = 'DECENT成立于2015年，是一家非营利性基金会，自主开发了开源的区块链DCore。 DECENT长期以来与顶级投资基金和孵化器保持着密切合作，致力于利用其专有的区块链技术构建DECENT生态，助力开发者和企业创造更有效率的分布式未来。\n\nDCore区块链于2017年启动，是一款稳定、可定制且成本友好的开源区块链平台。作为全球首个致力于数字内容分发、媒体和娱乐领域的区块链，DCore为分布式网络中的dApp开发者与企业应用提供用户友好的软件开发工具。\n\n了解更多信息请点击我们的官网：https://decent.ch/zh/';
    if (msg.chat.id == wg4 || msg.chat.id == groupId) {
        info = 'Founded in 2015, DECENT is a non-profit foundation that has developed the open source blockchain DCore. DECENT has long worked closely with top investment funds and incubators to build a DECENT ecosystem with its proprietary blockchain technology, enabling developers and businesses to create a more efficient and distributed future.\n\nLaunched in 2017, the DCore blockchain is a stable, customizable and cost-effective open source blockchain platform. As the world\'s first blockchain dedicated to digital content distribution, media and entertainment, DCore provides user-friendly software development tools for dApp developers and enterprise applications in distributed networks.\n\nFor more information, please click on our official website: https://decent.ch/';
    }
    bot.sendMessage(chatId, info), {disable_web_page_preview:true};
});

/**media链接**/
bot.onText(/^\/media/, (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let info = 'DECENT Social Media信息\n\n官方微信号：DECENTplatform\n投票工具：http://voting.decent.ch\n微信公众号：\n  1. DECENT区块链创新中心\n  2. DECENT爱好者\nTwitter：https://twitter.com/DECENTplatform';
    if (msg.chat.id == wg4 || msg.chat.id == groupId) {
        info = 'DECENT Social Media Info:\n\nReddit: reddit.com/r/Decentplatform\nTwitter: twitter.com/DECENTplatform\nInstagram: instagram.com/decentplatform\nLinkedIn: linkedin.com/company/decent-ngo-\nFacebook: facebook.com/DECENTplatform\nBitcointalk: bitcointalk.org/index.php?topic=1162392.0\n\nVoting for miners: voting.decent.ch/\n\nTelegram:\nSpanish - https://t.me//decentespanol\nChinese - https://t.me//decentchina\nRussian - https://t.me//DecentRu\n\nDCore Developers Telegram Group: https://t.me/DCoreDevelopers';
    }
    bot.sendMessage(chatId, info, {disable_web_page_preview:true});
});

/**需求合作**/
bot.onText(/^\/help/, (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let info = '请点击链接：https://decent.ch/zh/contact-us-cn/';
    if (msg.chat.id == wg4 || msg.chat.id == groupId) {
        info = 'Please click on this link:\nhttps://decent.ch/contact-us/';
    }
    bot.sendMessage(chatId, info, {disable_web_page_preview:true});
});

/**价格**/
const rp = require('request-promise')
const MARKET_EP = 'https://api.coinmarketcap.com/v2/ticker/1478';
bot.onText(/^\/price/, async (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let body = {
        method: 'GET',
        uri: MARKET_EP,
        json: true
    };
    let res = await rp(body)
    if(res.data) {
        bot.sendMessage(chatId, '$' + res.data.quotes.USD.price)
    }
});

/***MARKET CAP***/
bot.onText(/^\/test1/, async (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let body = {
        method: 'GET',
        uri: MARKET_EP,
        json: true
    };
    let res = await rp(body)
    if(res.data) {
        bot.sendMessage(chatId, res.data.quotes.USD.market_cap)
    }
});

/***CIRCULATING SUPPLY***/
bot.onText(/^\/test2/, async (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let body = {
        method: 'GET',
        uri: MARKET_EP,
        json: true
    };
    let res = await rp(body)
    if(res.data) {
        bot.sendMessage(chatId, res.data.circulating_supply)
    }
});


/***交易所***/
bot.onText(/^\/exchanges/, (msg, match) => {
    if(msg.chat.id != wg5 && msg.chat.id != wg4 && msg.chat.id != wg2 && msg.chat.id != groupId) return
    let chatId = msg.chat.id;
    let info = 'Bittrex\nUpbit\nChaoEX\nHitBTC\nBCEX\nRudex\nGbcax\nLbank\nBitpanther\nChangeNOW\nBit-Z\nCoinbene\n';
    bot.sendMessage(chatId, info);
});

/**签到功能**/
bot.onText(/^签到$/, (msg, match) => {
    // if(msg.chat.id != wg5) return
    let chatId = msg.chat.id; //group的ID
    let memberId = msg.from.id; //用戶的ID
    let name = msg.from.first_name ? msg.from.first_name : '' + msg.from.last_name ? msg.from.last_name : '';
    client.hgetall(`checkIn${msg.chat.id}`, (err, obj)=> {
        let dateString = (new Date()).toLocaleDateString();
        if( !obj || !obj[memberId] ) { //第一次签到
            let storeObj = {};
            storeObj[dateString] = 'true';
            storeObj.name = name;
            bot.sendMessage(chatId, name+'签到啦, '+'本月1次, 总共1次');
            client.hmset(`checkIn${msg.chat.id}`, memberId, JSON.stringify(storeObj));
        }
        else{ //之前已经签过到
            let check = JSON.parse(obj[memberId])
            
            if(check[dateString] === undefined) { //今天没有签到
                check[dateString] = 'true';
                let monthArray = dateString.split('-');
                monthArray[2] = '1';
                let month = monthArray.join('-');
                let totalCheck = Object.keys(check).length - 1;
                let monthCheck = Object.keys(check).filter(x => x>= month).length -1;
                bot.sendMessage(chatId, name+'签到啦, '+`本月${monthCheck}次, 总共${totalCheck}次`);
                client.hmset(`checkIn${msg.chat.id}`, memberId, JSON.stringify(check));
            }

            if(check[dateString]) { //今天已经签到
                let monthArray = dateString.split('-');
                monthArray[2] = '1';
                let month = monthArray.join('-');
                let totalCheck = Object.keys(check).length - 1;
                let monthCheck = Object.keys(check).filter(x => x>= month).length -1;
                bot.sendMessage(chatId, name+'签到啦, '+`本月${monthCheck}次, 总共${totalCheck}次`);
            }
        }
    });
})

bot.onText(/\/groupCheckIn (.+)/, (msg, match) => {
// if (msg.from.id != cm1 || msg.from.id != cm2 || msg.from.id != cm3 || msg.from.id != cm4 || msg.from.id != cm5)  return;
    let memberId = msg.from.id; //用戶的ID
    console.log(match[1]);
    console.log(match);
    client.hgetall(`checkIn${match[1]}`, (err, obj)=> {
        bot.sendMessage(memberId, JSON.stringify(obj))
    })
})

/**定时发送公告**/
client.hmset("banance", 'start', 'true');
const perday = '15:30:00'; //每天的15:30发送公告
const perTime = 1000 * 60 * 30; //每隔30分钟轮询 
const sendTitle = 'DECENT成立于2015年，是一家非营利性基金会，自主开发了开源的区块链DCore。 DECENT长期以来与顶级投资基金和孵化器保持着密切合作，致力于利用其专有的区块链技术构建DECENT生态，助力开发者和企业创造更有效率的分布式未来。\n\nDCore区块链于2017年启动，是一款稳定、可定制且成本友好的开源区块链平台。作为全球首个致力于数字内容分发、媒体和娱乐领域的区块链，DCore为分布式网络中的dApp开发者与企业应用提供用户友好的软件开发工具。\n\nDECENT官方微信号：DECENTplatform\nDECENT投票工具：http://voting.decent.ch\n微信公众号：\n  1. DECENT区块链创新中心\n  2. DECENT爱好者\n中文电报群：https://t.me/decentchina\n交易平台：Bittrex, BCEX, ChaoEX, HitBTC, Lbank, Upbit, G网(GBCAX)，Bit-Z，CoinBene\n合作伙伴：Venaco Group，蜻蜓科技，Film Independent';
const format = (timestamp) => {		 
    var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1)+'-' : date.getMonth()+1)+'-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    return Y+M+D+h+m+s;
};
const bannace = () => {
    let date = new Date();
    let dateString = format(date);
    let today = dateString.split(' ')[0] + ' ' + perday;
    client.hgetall('banance', (err, obj)=> {
        if(obj[today] === undefined) {
            client.hmset('banance', today, 'false');
        }

        if(obj[today] === 'false') {
            if ((new Date(today) - date) / perTime <= 1 && (new Date(today) - date) / perTime >= 0) {
                client.hmset('banance', today, 'true');
                setTimeout(()=> {
                    // TODO: 将groupId换成群组
                    // bot.sendMessage(wg5, sendTitle)
                    bot.sendMessage(groupId, sendTitle)
                }, new Date(today) - date)
            }
            if ((new Date(today) - date) / perTime < 0) {
                client.hmset('banance', today, 'true');
                // TODO: 将groupId换成群组
                // bot.sendMessage(wg5, sendTitle)
                bot.sendMessage(groupId, sendTitle)
            }
        }

        if(obj[today] === 'true') {
            // nothing...
        }
    })
}
// setInterval(bannace, perTime);


/*****群成员限制******/
bot.on('new_chat_members', msg => {
    // console.log(msg);
    console.log('加人了');

    // 邀请加入
    if(msg.from.id !== msg.new_chat_participant.id) {
        //console.log(msg.from.id + msg.from.first_name + msg.from.last_name)
        //console.log(msg.new_chat_participant.id + msg.new_chat_participant.first_name + msg.new_chat_participant.last_name)

        let storeObj = {
            from: msg.from.id,
            from_first_name: msg.from.first_name,
            from_last_name: msg.from.last_name,
            new_chat_member_id: msg.new_chat_participant.id,
            new_chat_member_first_name: msg.new_chat_participant.first_name,
            new_chat_member_last_name: msg.new_chat_participant.last_name
        }
        console.log(JSON.stringify(storeObj));

        client.hgetall('test01', (err, obj)=> {
            let from_value = obj && obj[msg.from.id]
            if(from_value) {
                let list = JSON.parse(from_value)
                let check = list.filter(new_chat => new_chat.new_chat_member_id !== msg.new_chat_participant.id)
                if(check.length == list.length) {
                    list.push(storeObj)
                    client.hset('test01', msg.from.id, JSON.stringify(list));
                }
            } else {
                client.hset('test01', msg.from.id, JSON.stringify([storeObj]));
            }
        })
    }

/*
    //新用户请在10分钟内私聊机器人完成入群测试
    msg.new_chat_members.forEach(member => {
        console.log(member);
        //let limit = {until_date: Math.round((Date.now() + 1000 * 60)/1000), can_send_messages:false, can_send_media_messages:false, can_send_other_messages:false, can_add_web_page_previews:false};
        bot.restrictChatMember(msg.chat.id, member.id, {can_send_messages:false, can_send_media_messages:false, can_send_other_messages:false, can_add_web_page_previews:false})
        bot.sendMessage(msg.chat.id,'新用户请在10分钟内私聊[机器人](https://t.me/etouyangBot)完成入群测试', {parse_mode:'Markdown'})
        setTimeout(()=> {
            bot.restrictChatMember(msg.chat.id, member.id, {can_send_messages:true, can_send_media_messages:true, can_send_other_messages:true, can_add_web_page_previews:true})
        }, 1000 * 30)
    })
*/


    //语言设置
    msg.new_chat_members.forEach(member => {
        if (member.is_bot)  return;
        let info = '';
        if(member.language_code.indexOf('es') == 0) {
            info = 'hey, I noticed your language is Spanish, feel free to join our Spanish Telegram group as well: link to Spanish group：https://t.me//decentespanol';
        }
        if(member.language_code.indexOf('ru') == 0) {
            info = 'hey, I noticed your language is Russian, feel free to join our Spanish Telegram group as well: link to Spanish group：https://t.me//DecentRu';
        }
        if (info !== '') {
            bot.sendMessage(member.id, info);
        }
    })
})

bot.onText(/\/start/, function (msg) {
    let chatId = msg.chat.id; //用戶的ID
    console.log(msg);
    let resp = '你好'; 
    let opts ={
        reply_markup: {
            inline_keyboard: [
                [{text:"不会", callback_data: "1"}],
                [{text:"会", callback_data: "2"}],
                [{text:"看心情", callback_data: "3"}]
            ]
        } 
        
    };
     bot.sendMessage(chatId,"如果有人问您索要账号密码、2FA、短信验证码, 您会给吗?",opts);


});

bot.on('callback_query', msg => {
    console.log(msg);
    let chatId = msg.from.id;
    let data = msg.data;

    if(data == '1') {
        bot.editMessageText('如果有人问您索要账号密码、2FA、短信验证码, 您会给吗?', {chat_id: chatId, message_id:msg.message.message_id})
        bot.sendMessage(chatId, '正确, 下一题');
        let opts ={
            reply_markup: {
                inline_keyboard: [
                    [{text:"这个是骗子", callback_data: "4"}],
                    [{text:"真的客服", callback_data: "5"}],
                    [{text:"我不知道", callback_data: "6"}]
                ]
            } 
            
        };
         bot.sendMessage(chatId,"如果在服务过程中, 客服问您索要报酬, 或者索要您的密码、2FA、短信等, 这个客服是?",opts);
    }

    if(data == '2' || data == '3') { 
        bot.editMessageText('如果有人问您索要账号密码、2FA、短信验证码, 您会给吗?', {chat_id: chatId, message_id:msg.message.message_id})
        bot.sendMessage(chatId, '答案不正确，请输入 /start 重新作答！');
    }

    if(data == '4') {
        bot.editMessageText('如果在服务过程中, 客服问您索要报酬, 或者索要您的密码、2FA、短信等, 这个客服是?', {chat_id: chatId, message_id:msg.message.message_id})
        bot.sendMessage(chatId, '您已全部作答正确, 可以正常参与讨论');
    }

    if(data == '5' || data =='6') {
        bot.editMessageText('如果在服务过程中, 客服问您索要报酬, 或者索要您的密码、2FA、短信等, 这个客服是?', {chat_id: chatId, message_id:msg.message.message_id})
        bot.sendMessage(chatId, '答案不正确，请输入 /start 重新作答！');
    }
})

//
bot.on('message', (msg) => {
    /**
{ message_id: 83,
  from: 
   { id: 574201003,
     is_bot: false,
     first_name: '欧阳',
     last_name: '青',
     language_code: 'zh-Hans-CN' },
  chat: 
   { id: -1001228652453,
     title: 'myTelegrameNode',
     username: 'testNodeTelegram',
     type: 'supergroup' },
  date: 1541049093,
  text: '测试' }
     */
    const chatId = msg.chat.id;
    console.log('---');
    console.log(msg);
    console.log('---');

    if(msg.document) {
        console.log('存在')
        let file_name = msg.document.file_name
        if(file_name.indexOf('exe') !== -1 || file_name.indexOf('src') !== -1) {
            console.log('删除')
            bot.deleteMessage(chatId, msg.message_id)
        }
    }

    //delete forward
    if(msg.forward_from) {
        bot.deleteMessage(chatId, msg.message_id)
    }

    // bot.sendMessage(chatId, 'Received your message');
  });

// import fs from 'fs';
// import xlsx from 'node-xlsx';

// const data = [['firstname', 'nastname', 'id', 'text', 'date'], ['小', '明', '17014'], [null,null,null,'你们decent的办公地点有哪些', '2018-11-19T14:30Z'],[null,null,null,'你们decent的办公地点有哪些', '2018-11-19T14:30Z'],[null,null,null,'你们decent的办公地点有哪些', '2018-11-19T14:30Z'],['你们decent的办公地点有哪些', '2018-11-19T14:30Z']];
// var buffer = xlsx.build([{name: "mySheetName", data: data}]); // Returns a buffer
// fs.writeFileSync('test1.xlsx',buffer,{'flag':'w'}); 


//decent
bot.onText(/^\/referral/, (msg, match) => {
    let chatId = msg.chat.id; //group id or user id
    let from_id = msg.from.id; // user id

    client.hgetall('test01', (err, obj)=> {
        console.log(obj)
        let from_value = obj && obj[from_id]
        if(from_value) {
            bot.sendMessage(chatId, from_value)
        } else {
            bot.sendMessage(chatId, 'no member you have referral')
        }
    })
})